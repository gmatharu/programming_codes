#include <stdio.h>
#include <stdlib.h>

//Function: Eliminates multiples of all numbers and returns oly prime numbers.
//Reference: using concept from Sieve of Eratosthenes
int sum_if_prime(number)
{
	//get all the numbers starting from 2
	unsigned int is_prime = 0;
	unsigned long long int sum = 0;
	unsigned int *ptr ;
    unsigned int *array_num = (unsigned int*)malloc(number*sizeof(unsigned int));
    //ptr = array_num;
    unsigned int a = 1;
    for(a =1;a<number;a++)
    {
    	//the values are stored starting from zero
    	//*ptr++ = 1;
    	array_num[a] = 1;
    }
        unsigned long long int i=2 ;
		unsigned long long int x=0 ;
		 //eliminate the multiples of each number and you will be left with prime numbers
		for(i=2;i < number;i++)
        {
        	 for(x=i;i*x < number;x++)
		         {
		            array_num[i*x]=0;
		         }
        }
        for(i=2;i < number;i++)
		    {
		        if(array_num[i] != 0)
		        {
		        	is_prime = i;
		            //printf("prime number :%u\n",is_prime);
		        	sum = sum + is_prime;
		        }

            }

    printf("Sum of prime numbers is: %llu\n",sum);
    return 0;
}

int main(int argc, char *argv[])
{
	unsigned int number = 0;
	printf("Please enter a number between 0 and 10,000,000\n");
	scanf("%d",&number);
	printf("The input number is: %d\n",number);
    //call function to check for prime number
    sum_if_prime(number);
    return 0;
}


